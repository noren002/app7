﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace app7
{
    public partial class MainPage : ContentPage
    {
        bool menureset = true;

        public MainPage()
        {
            InitializeComponent();
        }

        //clicking the hamburger menu button animates its view and enables its button functionality only when visible
        void goMenu(object sender, EventArgs e) 
        {
            if (menureset)
            {
                topbar.RotateTo(45, 1000);
                middlebar.RotateTo(-45, 1000);
                middlebar.TranslateTo(0, -10, 1000);
                bottombar.RotateTo(180, 1000);
                bottombar.TranslateTo(0, 50, 1000);
                bottombar.FadeTo(0, 500);
                menureset = false;
                menulist.FadeTo(1, 1000);
                aboutme.IsEnabled = true;
                mywork.IsEnabled = true;
                contactme.IsEnabled = true;
                facebook.IsEnabled = true;
                linkedin.IsEnabled = true;
                github.IsEnabled = true;

            }
            else
            {
                topbar.RotateTo(0, 1000);
                middlebar.RotateTo(0, 1000);
                middlebar.TranslateTo(0, 0, 1000);
                bottombar.RotateTo(-180, 1000);
                bottombar.TranslateTo(0, 0, 1000);
                bottombar.FadeTo(1, 500);
                menureset = true;
                menulist.FadeTo(0, 1000);
                aboutme.IsEnabled = false;
                mywork.IsEnabled = false;
                contactme.IsEnabled = false;
                facebook.IsEnabled = false;
                linkedin.IsEnabled = false;
                github.IsEnabled = false;
            }

        }

        async void GoFacebook(object sender, EventArgs e)//the next three functions highlight the button onclick and allows nav functionality
        {
            facebook.BackgroundColor = Color.Orange;
            Device.StartTimer(TimeSpan.FromSeconds(0.25), () =>
            {
                facebook.BackgroundColor = Color.Transparent;
                return false;
            });
            bool answer = await DisplayAlert("Exiting App", "Would You Like To Navigate To My Facebook?", "Okay", "Cancel");
            if (answer) Device.OpenUri(new Uri("https://www.facebook.com/jake.norenberg"));
            else return;
            
        }

        async void GoLinkedIn(object sender, EventArgs e)
        {
            linkedin.BackgroundColor = Color.Orange;
            Device.StartTimer(TimeSpan.FromSeconds(0.25), () =>
            {
                linkedin.BackgroundColor = Color.Transparent;
                return false;
            });
            bool answer = await DisplayAlert("Exiting App", "Would You Like To Navigate To Navigate To My LinkedIn?", "Okay", "Cancel");
            if (answer) Device.OpenUri(new Uri("https://www.linkedin.com/in/jacobnorenberg/"));
            else return;
            
        }

        async void GoGitHub(object sender, EventArgs e)
        {
            github.BackgroundColor = Color.Orange;
            Device.StartTimer(TimeSpan.FromSeconds(0.25), () =>
            {
                github.BackgroundColor = Color.Transparent;
                return false;
            });
            bool answer = await DisplayAlert("Exiting App", "Would You Like To Navigate To My BitBucket?", "Okay", "Cancel");
            if (answer) Device.OpenUri(new Uri("https://bitbucket.org/noren002/"));
            else return;
        }

        async void MenuNav(object sender, EventArgs e)//button navigation functionality and visuals
        {
            if (((Button)sender).Text == "About Me")
            {
                aboutme.TextColor = Color.Orange;
                Device.StartTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    aboutme.TextColor = Color.LightGray;
                    return false;
                });
                await Navigation.PushAsync(new AboutMe());
            }

            if (((Button)sender).Text == "My Work")
            {
                mywork.TextColor = Color.Orange;
                Device.StartTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    mywork.TextColor = Color.LightGray;
                    return false;
                });
                await Navigation.PushAsync(new MyWork());
            }

            if (((Button)sender).Text == "Contact Me")
            {
                contactme.TextColor = Color.Orange;
                Device.StartTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    contactme.TextColor = Color.LightGray;
                    return false;
                });
                await Navigation.PushAsync(new ContactMe());
            }
        }

    }
}
