﻿using System;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace app7
{
    public partial class ContactMe : ContentPage
    {
        public ContactMe()
        {
            InitializeComponent();
        }

        async void GoEmail(object sender, EventArgs e) //3 Functions for 3 button animations and warning messages
        {
            await email_btn.ScaleTo(1.1, 250);
            await email_btn.ScaleTo(1, 250);
            bool answer = await DisplayAlert("Switching To Email..", "Would You Like To Send Me An Email?", "Okay", "Cancel");
            if (answer) Device.OpenUri(new Uri("mailto:jacobn1003@gmail.com"));
            else return;
        }

        async void GoPhone(object sender, EventArgs e)
        {
            await phone_btn.ScaleTo(1.1, 250);
            await phone_btn.ScaleTo(1, 250);
            bool answer = await DisplayAlert("Switching To Contacts..", "Would You Like To Message or Call Me?", "Okay", "Cancel");
            if (answer) Device.OpenUri(new Uri("tel:7159219375"));
            else return;
        }

        async void GoAddress(object sender, EventArgs e)
        {
            await address_btn.ScaleTo(1.1, 250);
            await address_btn.ScaleTo(1, 250);
            Clipboard.SetTextAsync(address_btn.Text); //clicking the button copies my address
            if (Clipboard.HasText)
            {
                var text = await Clipboard.GetTextAsync();
                DisplayAlert("Copied.", text, "Ok");
            }
            return;
        }
    }
}
