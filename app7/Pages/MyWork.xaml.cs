﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace app7
{
    public partial class MyWork : ContentPage
    {
        public MyWork()
        {
            InitializeComponent();
            JakesPicker.Items.Add("Hw1 Getting Started");//hardcoding items to the picker list
            JakesPicker.Items.Add("Hw2 Calculator");
            JakesPicker.Items.Add("Hw3 TabPage");
            JakesPicker.Items.Add("Hw3 NavPage");
            JakesPicker.Items.Add("Hw4 ListView");
            JakesPicker.Items.Add("Hw5 Google Maps");
            JakesPicker.Items.Add("Hw6 Json API");
            
        }

        void Handle_Picker(System.Object sender, System.EventArgs e)
        {
            var name = JakesPicker.Items[JakesPicker.SelectedIndex]; //hardcoding endpoints for the webview to show
            if (name == "Hw1 Getting Started") webview.Source = "https://bitbucket.org/noren002/cs481_hw1/";
            if (name == "Hw2 Calculator") webview.Source = "https://bitbucket.org/noren002/cs481_hw2-calculator/";
            if (name == "Hw3 TabPage") webview.Source = "https://bitbucket.org/noren002/cs481_hw3-tabbed/";
            if (name == "Hw3 NavPage") webview.Source = "https://bitbucket.org/noren002/cs481_hw3-nav/";
            if (name == "Hw4 ListView") webview.Source = "https://bitbucket.org/noren002/cs481_hw4-listview/";
            if (name == "Hw5 Google Maps") webview.Source = "https://bitbucket.org/noren002/cs481_hw5-map/";
            if (name == "Hw6 Json API") webview.Source = "https://bitbucket.org/noren002/cs481_hw6-json/";
            
        }
    }
}
