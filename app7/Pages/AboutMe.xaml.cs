﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace app7
{
    public partial class AboutMe : ContentPage
    {
        public AboutMe()
        {
            InitializeComponent();
        }

        async void Go(object sender, EventArgs e) //contains the tab-like functionality to my employment section and hardcoded text
        {
            if (((Button)sender).Text == "NIWC")
            {
                await niwc.ScaleTo(1.2, 250);
                await niwc.ScaleTo(1, 250);
                niwc.BackgroundColor = Color.Orange;
                pizzaport.BackgroundColor = Color.DimGray;
                bestbuy.BackgroundColor = Color.DimGray;
                usmc.BackgroundColor = Color.DimGray;
                title_text.Text = "Intern [August 2019 - Present]";
                sub_text.Text = "Working on different projects associated with cyber security and web development on the Naval Research Base under the Naval Information Warfare Center. Developing frontend and backend functionality for different projects, mainly using Reactjs and Python Flask.";
            }
            if (((Button)sender).Text == "Pizza Port")
            {
                await pizzaport.ScaleTo(1.2, 250);
                await pizzaport.ScaleTo(1, 250);
                niwc.BackgroundColor = Color.DimGray;
                pizzaport.BackgroundColor = Color.Orange;
                bestbuy.BackgroundColor = Color.DimGray;
                usmc.BackgroundColor = Color.DimGray;
                title_text.Text = "Kitchen Assistant [August 2017 - August 2019]";
                sub_text.Text = "Gained experience on how to effectively communicate with multiple team members and personality types in a fast-paced work environment. Became efficient at multitasking and prioritizing small to large scale problems. Learned and taught the importance of attention to detail and punctuality.";
            }
            if (((Button)sender).Text == "Best Buy")
            {
                await bestbuy.ScaleTo(1.2, 250);
                await bestbuy.ScaleTo(1, 250);
                niwc.BackgroundColor = Color.DimGray;
                pizzaport.BackgroundColor = Color.DimGray;
                bestbuy.BackgroundColor = Color.Orange;
                usmc.BackgroundColor = Color.DimGray;
                title_text.Text = "Merchandiser-Visual Member [April 2016 - January 2017]";
                sub_text.Text = "Worked my way from being a supportive role on a team to being able to create my own way doing weekly tasks to maximize efficiency with minimal supervision. Trusted with making accurate calculations with numbers, data entry, and fixing electronics issues.";
            }
            if (((Button)sender).Text == "USMC")
            {
                await usmc.ScaleTo(1.2, 250);
                await usmc.ScaleTo(1, 250);
                niwc.BackgroundColor = Color.DimGray;
                pizzaport.BackgroundColor = Color.DimGray;
                bestbuy.BackgroundColor = Color.DimGray;
                usmc.BackgroundColor = Color.Orange;
                title_text.Text = "Manager/Instructor [January 2010 - January 2015]";
                sub_text.Text = "Taught classes of 35+ students each through lecture, guided discussion, and practical application. Managed a team of five Marines with a focus on personal and team development, increasing team proficiency by using communication skills and team building exercises. Directly applied precise data placement and assisted in a dynamic team effort. Developed and used specific communication skills needed to make accurate mathematical calculations and team-oriented decisions under pressure. Secret Security Clearance";
            }
        }
    }
}
